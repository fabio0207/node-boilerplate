require('dotenv').config();

const { existsSync } = require('fs');
const { join, resolve } = require('path');

/**
 * Helper utilizado em ~/src/config para tornar amigável a criação de arquivos
 * de configuração.
 *
 * @param {string} param
 * @param {any} defaultValue
 */
function env(param, defaultValue) {
    return process.env[param.toUpperCase()] || defaultValue;
}

/**
 * Helper utilizado no escopo geral da aplicação para acessar configurações
 * contidas em ~/src/config considerando o nome_arquivo.chave_principal.
 *
 * @param {string} param
 */
function config(param) {
    const [file, ...keys] = param.split('.');

    const rootPath = resolve(process.cwd(), 'src', 'config');
    const pathFile = join(rootPath, file + '.js');

    if (existsSync(pathFile)) {
        const configs = require(pathFile);

        if (keys) {
            return keys.reduce((object, key) => {
                return (object || {})[key];
            }, configs);
        }

        return configs;
    }
}

module.exports = { env, config };
