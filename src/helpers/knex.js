const _knex = require('knex');

const { driver, ...connectionData } = require('../config/database');

function knex() {
    try {
        if (!connectionData[driver]) {
            throw `Error: the '${driver}' driver does not exist.`;
        }

        if (
            !connectionData[driver].connection.filename &&
            !connectionData[driver].connection.database
        ) {
            throw `Error: database name not entered!`;
        }

        return _knex(connectionData[driver]);
    } catch (e) {
        console.log(e);

        process.exit(1);
    }
}

module.exports = knex();
