const { env } = require('../helpers/environment');

module.exports = {
    /**
     * Determina o escopo do ambiente de execução
     * @type {string}
     */
    env: env('APP_ENV', 'production'),

    /**
     * URL Base
     * @type {string}
     */
    url: env('APP_URL', 'http://localhost'),

    /**
     * Porta em que o serviço da aplicação ficará escutando
     * @type {number}
     */
    port: parseInt(env('APP_PORT', 3000)),

    /**
     * Fuso horário utilizado pela aplicação (Data/Hora)
     * @type {string}
     */
    timezone: 'America/Sao_Paulo',
};
