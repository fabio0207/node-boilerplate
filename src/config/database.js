const { env } = require('../helpers/environment');

module.exports = {
    driver: env('DB_CONNECTION', 'sqlite'),

    /**
    |---------------------------------------------------------------------------
    | SQLite
    |---------------------------------------------------------------------------
    |
    | npm install --save sqlite3
    |
    */
    sqlite: {
        client: 'sqlite3',
        connection: {
            filename: env('DB_DATABASE', ''),
        },
        useNullAsDefault: true,
        debug: env('DB_DEBUG', false),
    },

    /*
    |---------------------------------------------------------------------------
    | MySQL
    |---------------------------------------------------------------------------
    |
    | npm install --save mysql
    |
    */
    mysql: {
        client: 'mysql',
        connection: {
            host: env('DB_HOST', 'localhost'),
            port: env('DB_PORT', 3306),
            user: env('DB_USER', 'root'),
            password: env('DB_PASSWORD', ''),
            database: env('DB_DATABASE', ''),
        },
        debug: env('DB_DEBUG', false),
    },

    /*
    |---------------------------------------------------------------------------
    | PostgreSQL
    |---------------------------------------------------------------------------
    |
    | npm install --save pg
    |
    */
    pg: {
        client: 'pg',
        connection: {
            host: env('DB_HOST', 'localhost'),
            port: env('DB_PORT', 5432),
            user: env('DB_USER', 'root'),
            password: env('DB_PASSWORD', ''),
            database: env('DB_DATABASE', ''),
        },
        debug: env('DB_DEBUG', false),
    },
};
