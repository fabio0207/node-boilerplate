# node-boilerplate

Este repositório representa uma estrutura mínima e flexível sobre a qual novos projetos DEVERIAM ser criados. Sua principal função é padronizar aspectos gerais de formatação e configuração de elementos base.

> As palavras-chave "DEVE", "NÃO DEVE", "REQUER", "DEVERIA", "NÃO DEVERIA", "PODERIA", "NÃO PODERIA", "RECOMENDÁVEL", "PODE", e "OPCIONAL" neste documento devem ser interpretadas como descritas no [RFC 2119](http://tools.ietf.org/html/rfc2119). Tradução livre [RFC 2119 pt-br](http://rfc.pt.webiwg.org/rfc2119).

## Pré-requisito

Novos projetos DEVEM ser construídos considerando a versão LTS mais recente do NodeJS. Este repositório sugere o uso do:

- NODE (LTS) >= 14.17.5

## Como utilizar

Para clonar o repositório:
```
git clone git@bitbucket.org:fabio0207/node-boilerplate.git
```

Acesse o diretório do projeto:
```
cd node-boilerplate
```

Para instalar as dependências:
```
npm install
```

Para testar:
```
node src/index.js
```

> Output: `Porta default da aplicação: 3000`

Por fim é RECOMENDÁVEL "achatar" os commits, ou seja, "reinicializar" o log de alterações/trilha de commits e remover o apontamento para este repositório:
```
git reset $(git commit-tree HEAD^{tree} -m "Initial commit - based on https://bitbucket.org/fabio0207/node-boilerplate") && git remote remove origin
```

### Utilizando banco de dados

Este projeto utiliza o Knex como motor de query builder. Para fazer sua utilização basta instalar o driver do banco a ser utilizado. Ex:

```
npm install pg
npm install mysql
npm install sqlite
```

> Exemplos de configuração de ambiente podem ser encontrados no arquivo `.env-sample`.

Para utilizar recursos de banco basta adicionar a instrução abaixo no cabeçalho de import do método desejado:

```
const knex = require('../helpers/knex');
```

> Você DEVE informar o caminho relativo considerando seu escopo de trabalho. Um exemplo de uso está disponĩvel em `~/src/index.js`.

## Considerações

O diretório **.vscode** presente na raiz deste repositório possui algumas extensões e configurações tidas como sugeridas, logo é RECOMENDADO considerar a instalação das extensões e uso das configurações sugeridas.

> ***Obs***: ao abrir o projeto o VSCode irá sugerir no canto inferior direito a instalação das extensões registradas em `.vscode/extensions.json` e importe das configurações mapeadas em `.vscode/settings.json`.
